import List from '../components/List';

export default {
  data() {
    return {
      // FIXME: This should be true by default
      isLoading: false,
    };
  },
  components: {
    List,
  },
};
