import axios from 'axios';
import { STORAGE_KEY_PROJECT_ID } from '../store/constants.js';

const getProjectId = () => {
  return localStorage.getItem(STORAGE_KEY_PROJECT_ID);
}

const glResource = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
  headers: {
    'PRIVATE-TOKEN': localStorage.getItem('GL_TOKEN'),
  },
});

export default {
  // FIXME: Improve this
  fetchProjects({ commit }) {
    const USER_PATH = 'user';
    const PROJECTS_PATH = 'projects?starred=true&simple=true&order_by=last_activity_at';

    return glResource.get(USER_PATH)
      .then(userRes => {
        commit('setUser', userRes.data);

        glResource.get(PROJECTS_PATH)
        .then(projectRes => {
          commit('setProjects', projectRes.data);
            commit('toggleLoading');
          });
      });
  },
  fetchAllData({ dispatch, state }) {
    dispatch('fetchIssues');
    dispatch('fetchAssignedMRs');
    dispatch('fetchCreatedMRs');
    dispatch('fetchPipelines', state.user.username);
  },
  fetchIssues({ commit }) {
    const PROJECT_ID = getProjectId();
    const ISSUES_PATH = `projects/${PROJECT_ID}/issues?scope=assigned-to-me&state=opened`;

    return glResource.get(ISSUES_PATH).then(res => {
      commit('setIssues', res.data);
    });
  },
  fetchAssignedMRs({ commit }) {
    const PROJECT_ID = getProjectId();
    const ASSIGNED_MRS_PATH = `projects/${PROJECT_ID}/merge_requests?scope=assigned-to-me&state=opened`;

    return glResource.get(ASSIGNED_MRS_PATH).then(res => {
      commit('setAssignedMRs', res.data);
    });
  },
  fetchCreatedMRs({ commit }) {
    const PROJECT_ID = getProjectId();
    const CREATED_MRS_PATH = `projects/${PROJECT_ID}/merge_requests?scope=created-by-me&state=opened`;

    return glResource.get(CREATED_MRS_PATH).then(res => {
      commit('setCreatedMRs', res.data);
    });
  },
  fetchPipelines({ commit }, username) {
    const PROJECT_ID = getProjectId();
    const PIPELINES_PATH = `projects/${PROJECT_ID}/pipelines?username=${username}`;

    return glResource.get(PIPELINES_PATH).then(res => {
      commit('setPipelines', res.data);
    });
  },
};
