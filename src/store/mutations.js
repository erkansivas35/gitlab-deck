import { TYPES } from './constants'

export default {
  toggleLoading(state) {
    state.isLoading = !state.isLoading;
  },
  setProjects(state, projects) {
    state.projects = projects;
  },
  setUser(state, user) {
    state.user = user;
  },
  setIssues(state, issues) {
    state.issues = issues.map((issue) => {
      return { ...issue, dataType: TYPES.ISSUE };
    });
  },
  setAssignedMRs(state, mrs) {
    state.mrsAssignedToMe = mrs.map((mr) => {
      return { ...mr, dataType: TYPES.MERGE_REQUEST };
    });
  },
  setCreatedMRs(state, mrs) {
    state.mrsCreatedByMe = mrs.map((mr) => {
      return { ...mr, dataType: TYPES.MERGE_REQUEST };
    });
  },
  setPipelines(state, pipelines) {
    state.pipelines = pipelines.map((pipeline) => {
      return { ...pipeline, dataType: TYPES.PIPELINE };
    });
  },
};
