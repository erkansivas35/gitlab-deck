export default {
  isLoading: true,
  user: {},
  projects: [],
  issues: [],
  mrsAssignedToMe: [],
  mrsCreatedByMe: [],
  pipelines: [],
};
